@extends('layouts')

@section('content')
<head>
	<link rel="stylesheet" href="./public/css/styleHome.css">
</head>

<!-- // require('ProductController.php');
// require('database.php');
// require('Product.php'); -->
<h1 class="title">Listes des produits</h1>
@foreach ($products as $product) 

<section class="container">

		<div class="columns">
			<div class="card">
				<div class="card-image">
					<figure class="image is-2by5">
						<img src="{{$product->picture}}" alt="Picture product">
					</figure>
				</div>
				<div class="card-content">
					<div class="media">
						<div class="media-content">
							<p class="title is-4"><a href="/product/{{$product->id}}">{{$product->name}}</a></p>
							<p class="subtitle is-6">Prix: {{$product->price}}€</p>
						</div>

					</div>
				</div>
			</div>
		</div>

	@endforeach

	</section>
@endsection