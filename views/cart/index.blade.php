@extends('layouts')

@section('content')
<section class="container">
	<h1 class="title">Mon panier</h1>

	<hr>
	<table class="table is-striped is-narrow is-hoverable is-fullwidth">
		<thead>
			<tr>
				<th>Articles</th>
				<th>Prix</th>
				<th>Quantité</th>
			</tr>
		</thead>
		<tbody>

			@foreach ($panier as $key => $quantity)
			<tr>
				<td>{{ $products[$key-1]->name }}</td>
				<td>{{ $products[$key-1]->price }}</td>
				<td>{{ $quantity }}</td>
			</tr>
			@endforeach

			{{-- Blade : Boucles pour récupérer des produits commander ( https://laravel.com/docs/5.8/blade), 
							 Bulma : https://bulma.io/documentation/elements/table/--}}
		</tbody>
		<tfoot>
			<tr>
				<th>Articles</th>
				<th>Prix</th>
				<th>Quantité</th>
				<th>
					{{-- Afficher le prix totals de tout les produits --}}
					<h3 class="subtitle is-5">Total (€) : {{ $total }} &euro;</h3>
					{{-- afficher le nombre de produits aux totals --}}
					<h3 class="subtitle is-5">Nombre de produits : {{ $nbproduct }}</h3>
					<div class="buttons">
						<a href="/" class="button is-small is-default">Retour au shopping</a>
						<a href="/order" class="button is-small is-success">Valider la commande</a>
					</div>
				</th>
			</tr>
		</tfoot>
	</table>
</section>
@endsection
