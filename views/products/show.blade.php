@extends('layouts')

@section('content')

<div class="card">
				<div class="card-image">
					<img src="{{$product->picture}}" alt="Picture product">	
				</div>
				<div class="card-content">
					<div class="media">
						<div class="media-content">
							<p class="title is-4">Nom: {{$product->name}}</p>
							<p class="subtitle is-6">Prix: {{$product->price}}€</p>
						</div>
					</div>
					<form action="/cart/add" method="post">
					<input type="hidden" name="idProduit" value="{{$product->id}}">
					<!-- <input type="hidden" name="nomProduit" value="{{$product->name}}">
					<input type="hidden" name="prixProduit" value="{{$product->price}}">
					<input type="hidden" name="action" value="add"> -->
					<div class="columns">
						<div class="column is-one-fifth">
							Quantité: <input class="input is-rounded" type="number" min="1" name="nbArticle" placeholder="Nb d'articles">
						</div>
					</div>
					<button type="submit" name="add" class="button is-danger">Ajouter au panier</button>
					</form>
				</div>


	
	{{-- 
			Affiche toute les information d'un seul produit
			avec un bouton pour ajouter le produits et on peut choisir la quantité
	--}}
@endsection