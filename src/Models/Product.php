<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model 
{
		/**
	 * Les propriéter éditable de la table products
	 * @var array
	 */
	
	protected $fillable = ['title'];
	/**
	 * désactive le timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * OPTIONAL
	 * Les propriéter éditable de la table orders
	 * @var array
	 * @TIP : https://laravel.com/docs/5.8/eloquent-relationships#defining-relationships
	 */
	public function orders() {}
}