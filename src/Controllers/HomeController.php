<?php 

namespace App\Controllers;

use App\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Services\Cart;

/**
 * Controller pour l'home
 */
class HomeController extends Controller {
	/**
	 * Affiche la page du magasin avec tout les produits
	 * @return view retourne la vue avec tout les produits
	 */
	public function index(){
		// return "Something Wrong ! Check your HomeController";
		$p = Product::all();
		return view('home', ['products' => $p]);
	}

	/**
	 * Destroy la session php
	 * @return view redirige vers la view home
	 */
	public function logout(){

	}
}