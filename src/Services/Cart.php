<?php

namespace App\Services;

use App\Models\Product;

class Cart
{
	/**
	 * Retourne le tableau produits ajouter par l'utilisateur
	 * @return array Data Object
	 */
	public static function get(){
		return [];
	}

	/**
	 * Ajoute un produits dans le paniers
	 */
	public static function add($product, $quantity){
		if(!is_array($_SESSION["panier"])) {
			$_SESSION["panier"] = [];
		}
		$_SESSION["panier"][$product] += $quantity;
	}

	/**
	 * Compte le nombre d'article qu'il y à dans le panier
	 */
	public function count(){
		$nbproduct = 0;
		foreach ($_SESSION["panier"] as $quantity) {
			$nbproduct += $quantity;
		}
		return $nbproduct;
	}

	/**
	 * Calcul le prix total du panier
	 */
	public function total(){
		$total = 0;
		foreach ($_SESSION["panier"] as $key => $quantity) {
			$total += Product::find($key)->price * $quantity;
		}
		return $total;
	}
}
